var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

const config = {
  llave: "calveseguridad1812"
}

var path = require('path');
var requestjson = require('request-json');
var movimientosJSON = require('./movimientos2.json')
var bodyparser = require('body-parser');
var nodeMailer = require('nodemailer');
var jwt = require('jsonwebtoken');
var bcrypt = require("bcryptjs");
//var urlClientes = "https://api.mlab.com/api/1/databases/lreyes/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
//var clienteMLab = requestjson.createClient(urlClientes);

var raizMlab = "https://api.mlab.com/api/1/databases/lreyes/collections/";
var keyMlab = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var BCRYPT_SALT_ROUNDS = 12;

var smtpTransport = nodeMailer.createTransport({
  service: "Gmail",
  auth: {
    user: "practitioner3edmx@gmail.com",
    pass: "itoisa2013"
  }
});
var rand, mailOptions, host, link;
const rutasProtegidas = express.Router();

app.set('llave', config.llave);
app.use(bodyparser.urlencoded({ extended: true}));
app.use(bodyparser.json());

app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, access-token, Cross-Origin, Authorization, X-API-KEY, Access-Control-Allow-Request-Method");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
})

app.listen(port);

rutasProtegidas.use( function(req, res, next){
  const token = req.headers['access-token'];
  console.log("en rutas protegidas")
  if(token){
    jwt.verify(token, app.get('llave'),(err, decoded) => {
      if(err){
        console.log(err);
        return res.json({mensaje: 'Token inválido'});
      } else{
        req.decoded = decoded;
        next();
      }
    });
  }else{
    console("sin token")
    res.send({
      mensaje: 'no se ha enviado un token'
    });
  }
});

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res, otro){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.get('/send/:email', function(req, res){
  rand = Math.floor((Math.random() * 100) + 54);
  host = req.get('host');
  link = "https://ec2-3-16-21-100.us-east-2.compute.amazonaws.com/verify?id="+rand;
  mailOptions={
    to : req.params.email,
    subject : "please confirm your Email Account",
    html : "Bienvenido! <br> Para verificar tu cuenta da click Aquí link<br><a href="+link+">Aquí</a>"
  }
  console.log(mailOptions);
  smtpTransport.sendMail(mailOptions, function(error, response){
    if(error){
      console.log(error)
      res.send(500,error.message);
    }else{
      console.log("Mensaje enviado: " + response);
      res.send({mensaje: "email enviado"})
    }
  })
})

app.get('/verify', function(req, res){
  console.log(req.protocol+":/"+req.get('host'));
  if((req.protocol+"://"+req.get('host')) == ("http://"+host)){
    console.log("Dominio correcto");
    if(req.query.id == rand){
      console.log("Email correcto");
      var urlUsuarios = raizMlab + 'Usuarios' + keyMlab+'&q={email: "'+ mailOptions.to +'"}';
      var clienteMLabRaiz = requestjson.createClient(urlUsuarios);
      var newBody = { $set: {verificado: true}};
      clienteMLabRaiz.put('', newBody, function(err, resM, body){
        if(!err){
          console.log(body)
          res.sendFile(path.join(__dirname, 'verificacion.html'));
        }else{
          res.status(404).send({error: "usuario no verificado"})
          console.log(err);
        }
      })
    }else{
      console.log("email is not verified");
        res.send({mensaje: "Bad Request"});
    }
  }else{
    res.send({mensaje: "Request is from unknown source"});
  }
})

app.get("/Clientes", rutasProtegidas, function (req, res){
  var urlClientes = raizMlab + "Clientes" + keyMlab;
  var clienteMLabRaiz = requestjson.createClient(urlUsuarios);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      res.send(body);
    }else{
      console.log(err);
    }
  })
})

app.post("/Clientes", function (req, res){
  var urlUsuarios = raizMlab + "Clientes" + keyMlab;
  var clienteMLabRaiz = requestjson.createClient(urlUsuarios);
  clienteMLabRaiz.post('', req.body, function(err, resM, body){
    res.send(body);
  })
})

app.get('/Movimientos/:numeroCuenta', rutasProtegidas, function(req, res){
  var urlMovimientos = raizMlab + 'Clientes' +keyMlab + '&q={"numeroCuenta": "'+req.params.numeroCuenta+'"}';
  var clienteMLabRaiz = requestjson.createClient(urlMovimientos);
  clienteMLabRaiz.get('', req.body, function(err, resM, body){
    if(!err){
      if(body.length == 1){
        console.log(body)
        res.send(body[0]);
      }else{
        res.send({mensaje: "Usuario No Encontrado"})
      }
    }else{
      console.log(err)
    }
  })
})

/*app.post('/Movimientos', rutasProtegidas, function(req, res){
  var urlMovimientos = raizMlab + 'Movimientos' +keyMlab;
  console.log(urlMovimientos);
  var clienteMLabRaiz = requestjson.createClient(urlMovimientos);
  clienteMLabRaiz.post('', req.body, function(err, resM, body){
    res.send(body);
  })
})*/

app.post("/Login", function(req, res){
  var email = req.body.email
  var password = req.body.password
  var query = 'q={"email": "'+email+'"}'
  clienteMLabRaiz = requestjson.createClient(raizMlab+ "Usuarios" + keyMlab+'&'+ query);
  console.log(clienteMLabRaiz);

  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length == 1){
        console.log(body);
        bcrypt.compare(password, body[0].password, function(err, result) {
          if(result){
            const payload = {
              check: true
            };
            const token = jwt.sign(payload, app.get('llave'),{
              expiresIn: 1440
            });
            console.log("Autenticado")
            res.status(200).send({
              mensaje : "Autenticacion correcta",
              token : token,
              usuario : body[0]
            });
          }else{
            console.log("contraseña incorrecta")
            res.status(404).send("Contraseña incorrecta")
          }
        });
      }else{
        res.status(404).send("usuario no encontrado")
      }
    }else{
      console.log("respondiendo erro: "+err);
    }
  })
})

app.post("/Registro", function(req, res){
  var urlUsuarios = raizMlab + "Usuarios" + keyMlab;
  var clienteMLabRaiz = requestjson.createClient(urlUsuarios);
  var pass = req.body.password;
  bcrypt.hash(pass, BCRYPT_SALT_ROUNDS, function(error, hash){
    if(!error){
      req.body.password = hash;
      clienteMLabRaiz.post('', req.body, function(err, resM, body){
        if(!err){
          console.log(body)
          res.send(body);
        }else{
          res.status(404).send("usuario no registrado")
          console.log(err);
        }
      })
    }else{
      console.log(error)
      res.status(500).send("Error interno de servidor");
    }
  })

})

app.put("/Usuarios/:email", function(req, res){
  var urlUsuarios = raizMlab + 'Usuarios' + keyMlab+'&q={email: "'+ req.params.email +'"}';
  var clienteMLabRaiz = requestjson.createClient(urlUsuarios);
  var newBody = { $set: req.body};
  clienteMLabRaiz.put('', newBody, function(err, resM, body){
    if(!err){
      console.log(body)
      res.send(body);
    }else{
      res.status(404).send("usuario no modificado")
      console.log(err);
    }
  })
})

app.put("/Movimientos/:numeroCuenta", rutasProtegidas, function(req, res){
  var urlMovimientos = raizMlab + 'Clientes' + keyMlab+'&q={numeroCuenta: "'+ req.params.numeroCuenta +'"}';
  var clienteMLabRaiz = requestjson.createClient(urlMovimientos);
  var newBody = { $set: req.body};
  clienteMLabRaiz.put('', newBody, function(err, resM, body){
    if(!err){
      console.log(body)
      res.send(body);
    }else{
      res.status(404).send("usuario no modificado")
      console.log(err);
    }
  })
})
